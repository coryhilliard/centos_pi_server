# Creating a CentOS server out of a Raspberry Pi


## Introduction

This is a quick tutorial, to build a CentOS server out of a Raspberry Pi.

I am giving this tutorial away for free as I want to allow people to leapfrog over my work, and possibly make this tutorial better for everyone.

If you have any ideas to make this better, please send me a merge request.  I'll be happy to consider it.


## License: GNU GPL v3.0

[![GNU GPL v3.0](http://www.gnu.org/graphics/gplv3-127x51.png)](http://www.gnu.org/licenses/gpl.html)

GNU site <http://www.gnu.org/licenses/gpl.html>.

